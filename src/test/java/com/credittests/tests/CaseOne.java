package com.credittests.tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.NoSuchElementException;



public class CaseOne extends Test {
    public void perform(){
        try{
            //Слайдер
            driver.findElement(By.xpath("//span[@class='_3ZIWw _1FAsH _1NDp-']"));
            //Сумма
            driver.findElement(By.xpath("//div[@class='_2IScz']/div[@class='_1hL_D _2jVhc _3yI-w']/input[@class='_33GIb']"));
            //Валюта
            driver.findElement(By.xpath("//div[@class='_2wH4v']/div[1]/div[1]/div[1]/div[1]")).click();
            //выпадающий список валют
            driver.findElement(By.xpath("//div[@class='_2wH4v']/div/div/div[2]/div/div[1]/ul"));
            //Срок кредита
            driver.findElement(By.xpath("//div[@class='sc-AxirZ cYDulI']")).click();
            //Выпадающий список выбора срока
            driver.findElement(By.xpath("//div[@class='sc-AxirZ dmFdia']/div[1]/div/div/div[2]/div/div[1]/ul"));
            //Ставка
            driver.findElement(By.xpath("//div[@data-test='input-rate']/div[1]/input"));
            //Кнопка Дальше
            driver.findElement(By.xpath("//div[@class='border-top padding-bottom-default padding-top-medium']/div[1]/div[2]/a"));
            //Строка с предложением
            driver.findElement(By.xpath("//div[@class='border-top padding-bottom-default padding-top-medium']/div[1]/div[1]/div[1]/b"));
            setResult(true);
        }catch (NoSuchElementException exception){
            setResult(false);
        }
        end();
           }
}
