package com.credittests.tests;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFTable;

public class TestRunner {
    public static void main(String[] args) {
        if(args.length > 0){
            String red = "FF0000";
            String green = "00FF00";
            Test testCase;
            try {
                WordWorker ww = new WordWorker();
                XWPFDocument document = ww.readFile("TestCases.docx");
                XWPFTable table = document.getTables().get(0);
                switch (args[0]) {
                    case "CaseOne" -> {
                        testCase = new CaseOne();
                        testCase.perform();
                    }
                    case "CaseTwo" -> {
                        testCase = new CaseTwo();
                        testCase.perform();
                    }
                    case "CaseThree" -> {
                        testCase = new CaseThree();
                        testCase.perform();
                    }
                    case "CaseFour" -> {
                        testCase = new CaseFour();
                        testCase.perform();
                    }
                    default -> {
                        testCase = new CaseOne();
                        testCase.perform();
                        if (testCase.getResult())
                            ww.fillCell(table, 1, 6, green);
                        else
                            ww.fillCell(table, 1, 6, red);
                        testCase = new CaseTwo();
                        testCase.perform();
                        if (testCase.getResult())
                            ww.fillCell(table, 2, 6, green);
                        else
                            ww.fillCell(table, 2, 6, red);
                        testCase = new CaseThree();
                        testCase.perform();
                        if (testCase.getResult())
                            ww.fillCell(table, 3, 6, green);
                        else
                            ww.fillCell(table, 3, 6, red);
                        testCase = new CaseFour();
                        testCase.perform();
                        if (testCase.getResult())
                            ww.fillCell(table, 4, 6, green);
                        else
                            ww.fillCell(table, 4, 6, red);
                    }
                }
                ww.writeFile(document, "results.docx");

            }catch (Exception ignored){}


        }
    }

}
