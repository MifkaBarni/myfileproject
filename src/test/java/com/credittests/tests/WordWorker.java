package com.credittests.tests;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xwpf.usermodel.IBodyElement;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFTable;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

public class WordWorker {
    public XWPFDocument readFile(String filePath) throws IOException, InvalidFormatException {
        FileInputStream inputFile = new FileInputStream(filePath);
        XWPFDocument document = new XWPFDocument(OPCPackage.open(inputFile));
        inputFile.close();
        return document;
    }
    public void writeFile(XWPFDocument document, String filePath) throws IOException {
        document.write(new FileOutputStream(filePath));
        document.close();
    }
    public boolean fillCell(XWPFTable table, int row, int col, String color) {
        {
            boolean result = false;
            try {
//                Iterator bodyElementIterator = xdoc.getBodyElementsIterator();
//                while (bodyElementIterator.hasNext()) {
//                    IBodyElement element = (IBodyElement) bodyElementIterator.next();
//                    if ("TABLE".equalsIgnoreCase(element.getElementType().name())) {
//                        List<XWPFTable> tableList = element.getBody().getTables();
//                        if (tableList.size() != 0) {
//                            XWPFTable table = tableList.get(0);
                            if (row < table.getNumberOfRows()) {
                                if (col < table.getRow(row).getTableICells().size()) {
                                    table.getRow(row).getCell(col).setColor(color);
                                    if (table.getRow(row).getCell(col).getColor().equals(color))
                                        result = true;
                                }
                            }
//
//                        }
//                    }
//                }
//                xdoc.write(new FileOutputStream("results.docx"));
//                xdoc.close();

            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return result;
        }
    }
}
