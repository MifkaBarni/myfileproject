package com.credittests.tests;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

public class CaseFour extends Test {
    public void perform() {

        boolean result = true;
        try {
            WebElement summa = driver.findElement(By.className("_33GIb"));
            summa.sendKeys(Keys.CONTROL + "a");
            summa.sendKeys(Keys.DELETE);
            summa.click();
            summa.sendKeys( "300000");
            driver.findElement(By.xpath("//div[@class='sc-AxirZ cYDulI']")).click();
            driver.findElement(By.xpath("//div[text()='3 месяца']")).click();
            WebElement stavka = driver.findElement(By.xpath("//div[@data-test='input-rate']/div[1]/input"));
            stavka.sendKeys(Keys.CONTROL + "a");
            stavka.sendKeys("10");
            WebElement kolvo = driver.findElement(By.xpath("//div[@class='_3rTsu _2tTnl _3vGph _2BOKl w7MPo _3biYo _1biMN _3P3Yi']/span[contains(text(),'кредитов подобрано')]"));
            String kolvoStr = kolvo.getText();
            String[] stringArray = kolvoStr.split(" ");
            if (Integer.parseInt(stringArray[0]) != 100) result = false;
            setResult(result);
        } catch (Exception exception) {
            setResult(false);
        }
        end();
    }
}
