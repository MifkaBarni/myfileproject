package com.credittests.tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


import java.util.concurrent.TimeUnit;

public class Test {
    private boolean result = false;
    protected WebDriver driver;

    public void setResult(boolean result) {
        this.result = result;
        System.out.println(result);
    }

    public boolean getResult() {
        return result;
    }

    public void perform(){

    }
    public void end(){
        driver.close();
    }
    public Test(){
        System.setProperty("webdriver.chrome.driver", "C:\\Program Files\\selenium-java.jar\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("https://www.banki.ru/services/calculators/credits/");
    }
}
