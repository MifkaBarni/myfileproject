package com.credittests.tests;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class CaseTwo extends Test{
    public void perform(){
        boolean status = true;
        try {
            int value = 300000;
            WebElement summa = driver.findElement(By.xpath("//div[@class='_2IScz']/div[@class='_1hL_D _2jVhc _3yI-w']/input[@class='_33GIb']"));
            WebElement slider = driver.findElement(By.xpath("//span[@class='_3ZIWw _1FAsH _1NDp-']"));
            String sliderPos1 = slider.getCssValue("left");
            summa.sendKeys(Keys.CONTROL + "a");
            summa.sendKeys(Keys.DELETE);
            summa.click();
            summa.sendKeys( String.valueOf(value));
            String sliderPos2 = slider.getCssValue("left");
            if(sliderPos1.equals(sliderPos2)) status = false;
            WebElement stroka = driver.findElement(By.xpath("//div[@class='border-top padding-bottom-default padding-top-medium']/div[1]/div[1]/div[1]/b"));
            String strokaSumma = stroka.getText();
            int strokaInt = Integer.parseInt(strokaSumma.substring(0,strokaSumma.length()-2).replace(" ", ""));
            if(value != strokaInt) status = false;
            setResult(status);
        }catch (Exception exception){
            setResult(false);
        }
        end();
    }
}
