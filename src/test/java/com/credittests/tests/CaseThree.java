package com.credittests.tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

public class CaseThree extends Test {
    public void perform() {
        try {
            boolean status = true;
            WebElement slider = driver.findElement(By.xpath("//span[@class='_3ZIWw _1FAsH _1NDp-']"));
            WebElement sliderContainter = driver.findElement(By.className("_193d-"));

            Actions action = new Actions(driver);
            Action test = action.dragAndDropBy(slider, sliderContainter.getSize().getWidth(), 0).build();
            test.perform();
            WebElement stroka = driver.findElement(By.xpath("//div[@class='border-top padding-bottom-default padding-top-medium']/div[1]/div[1]/div[1]/b"));
            String strokaSumma = stroka.getText();
            int strokaInt = Integer.parseInt(strokaSumma.substring(0, strokaSumma.length() - 2).replace(" ", ""));
            WebElement summa = driver.findElement(By.xpath("//div[@class='_2IScz']/div[@class='_1hL_D _2jVhc _3yI-w']/input[@class='_33GIb']"));
            String summaStr = summa.getAttribute("value");
            int summaInt = Integer.parseInt(summaStr.replace(" ", ""));
            if (summaInt != 100000000 || strokaInt != 100000000) status = false;
            setResult(status);
        } catch (Exception exception) {
            setResult(false);
        }
        end();
    }
}
